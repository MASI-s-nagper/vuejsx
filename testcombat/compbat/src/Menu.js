import React from 'react'
import {Link,NavLink} from 'react-router-dom';

function Menu() {
  return (
    

<nav className="navbar navbar-expand-sm navbar-dark bg-dark">
  <Link className="navbar-brand" to="/"><a href ="/"> <img src= "./assets/images/logo12.png" alt="logo" height="70"></img></a></Link>
  <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span className="navbar-toggler-icon"></span>
  </button>
  <div className="collapse navbar-collapse" id="navbarNav">
    <ul className="navbar-nav ml-auto" >
     
     <li className="nav-item">
        <NavLink  className="nav-link" to="/Register">Register</NavLink >
      </li>
      <li className="nav-item">
        < NavLink  className="nav-link" to="/Connection">Connection</ NavLink>
      </li>
      <li class="nav-item">
        <NavLink  className="nav-link" to ="/Welcome">Welcome </NavLink >
      </li>
     
    </ul>
  </div>
</nav>
      
    
  )
}

export default Menu;
