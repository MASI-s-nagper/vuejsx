import logo from './logo.svg';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import {BrowserRouter,Route,Switch} from 'react-router-dom';
import Welcome from './component/Welcome';
import Connection from './component/Connection';
import Register from './component/Register';
import Erreupage from './component/Erreupage';
import Menu from './Menu';


function App() {
  return (
    <BrowserRouter>
      <Menu></Menu>
      <Switch>
      <Route exact path ="/Welcome" component={Welcome}/>
      <Route path ="/Register" component={Register}/>
      <Route path ="/Connection" component={Connection}/>
      <Route component={Erreupage}/>
         
      </Switch>
     
    </BrowserRouter>
  );
}

export default App;
